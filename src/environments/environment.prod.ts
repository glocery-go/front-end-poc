const urlLogout = 'https://fg.ciam.preprod.aws.scania.com/auth/realms/scania/protocol/openid-connect/logout?redirect_uri=';
const redirectUri = encodeURI('https://localhost:4200');

export const environment = {
  production: true,
  apiUrl: '$API_URL',
  apiTaktUrl: '$API_TAKT_URL',
  apiWebLes: '$API_WEB_LES',
  imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
  apiDrun: 'https://directrun-api.br.scania.com',
  redirects: {
    redirectUrlSignIn: '$KEYCLOAK_REDIRECTURLSIGNIN'
  },

  keycloak: {
    url: '$KEYCLOAK_URL',
    urlLogout: '$KEYCLOAK_URL_LOGOUT',
    realm: 'scania',
    clientId: '$KEYCLOAK_CLIENTID',
  }
};


// export const environment = {
//   production: true,
//   apiUrl: 'https://acp-digitalqlist-api.br.scania.com',
//   apiTaktUrl: 'https://scada-cha-api.br.scania.com/api',
//   apiWebLes: 'https://acp-webles-api.br.scania.com',
//   imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
//   redirects: {
//     redirectUrlSignIn: 'https://acp-digitalqlist.br.scania.com'
//   },

//   keycloak: {
//     url: 'https://fg.ciam.preprod.aws.scania.com/auth',
//     urlLogout: urlLogout + redirectUri,
//     realm: 'scania',
//     clientId: 'sla_dqlist_consumer_acp',
//   }
// };



// export const environment = {
//   production: true,
//   apiUrl: 'https://digitalqlist-api.br.scania.com',
//   apiTaktUrl: 'https://scada-cha-api.br.scania.com/api',
//   apiWebLes: 'https://webles-api.br.scania.com',
//   imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
//   redirects: {
//     redirectUrlSignIn: 'https://digitalqlist.br.scania.com'
//   },

//   keycloak: {
//     url: 'https://fg.ciam.prod.aws.scania.com/auth',
//     urlLogout: urlLogout + redirectUri,
//     realm: 'scania',
//     clientId: 'sla_digitalqlist_consume_prod',
//   }
// };
