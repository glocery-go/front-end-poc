const urlLogout = 'https://fg.ciam.preprod.aws.scania.com/auth/realms/scania/protocol/openid-connect/logout?redirect_uri=';
const redirectUri = encodeURI('https://localhost:4200');

export const environment = {
  production: false,
  apiUrl: 'https://dev-digitalqlist-api.br.scania.com',
  apiTaktUrl: 'https://dev-scada-cha-api.br.scania.com/api',
  imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
  redirects: {
    redirectUrlSignIn: 'https://dev-digitalqlist.br.scania.com'
  },

  keycloak: {
    url: 'https://fg.ciam.preprod.aws.scania.com/auth',
    urlLogout: urlLogout + redirectUri,
    realm: 'scania',
    clientId: 'sla_digitalqlist_consume_dev',
  }
};
