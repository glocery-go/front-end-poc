
const urlLogout = 'https://fg.ciam.preprod.aws.scania.com/auth/realms/scania/protocol/openid-connect/logout?redirect_uri=';
const redirectUri = encodeURI('https://localhost:4200');

// export const environment = {
//   production: false,
//   apiUrl: 'https://acp-digitalqlist-api.br.scania.com',
//   apiTaktUrl: 'https://cada-cha-api.br.scania.com/api',
//   apiWebLes: 'https://webles-api.br.scania.com',
//   image_url: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
//   redirects: {
//     redirectUrlSignIn: 'https://localhost:4200'
//   },

//   keycloak: {
//     url: 'https://fg.ciam.preprod.aws.scania.com/auth',
//     urlLogout: urlLogout + redirectUri,
//     realm: 'scania',
//     clientId: 'sla_lfc_consumer_local',
//   }
// };


// export const environment = {
//   production: false,
//   apiUrl: 'http://digitalqlist-api.br.scania.com',
//   apiTaktUrl: 'https://scada-cha-api.br.scania.com/api',
//   apiWebLes: 'https://webles-api.br.scania.com',
//   imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
//   apiDrun: 'http://localhost:55086',
//   redirects: {
//     redirectUrlSignIn: 'https://localhost:4200'
//   },

//   keycloak: {
//     url: 'https://fg.ciam.preprod.aws.scania.com/auth',
//     urlLogout: urlLogout + redirectUri,
//     realm: 'scania',
//     clientId: 'sla_lfc_consumer_local',
//   }
// };


export const environment = {
  production: false,
  apiUrl: 'http://localhost:5000',
  apiTaktUrl: 'https://scada-cha-api.br.scania.com/api',
  apiWebLes: 'https://webles-api.br.scania.com',
  imageUrl: 'https://listatelefonica.br.scania.com/api/v1/listatelefonica/list?filter.entity.userId=',
  apiDrun: 'https://directrun-api.br.scania.com',
  redirects: {
    redirectUrlSignIn: 'https://localhost:4200'
  },

  keycloak: {
    url: 'https://fg.ciam.preprod.aws.scania.com/auth',
    urlLogout: urlLogout + redirectUri,
    realm: 'scania',
    clientId: 'sla_lfc_consumer_local',
  }
};

