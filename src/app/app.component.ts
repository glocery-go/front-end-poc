import { Component } from '@angular/core';
import { PwaUpdateService } from './pwa-update';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private pwa: PwaUpdateService,
  ) {
    this.pwa.checkForUpdates();
  }
}
