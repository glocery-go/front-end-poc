
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CameraComponent } from './camera/camera.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { FlipModule } from 'ngx-flip';

@NgModule({
  declarations: [
    CameraComponent
  ],
  exports: [
    CameraComponent
  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IonicSelectableModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    FlipModule
  ]
})
export class ComponentsModule { }
