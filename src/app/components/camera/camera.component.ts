import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';

interface CameraEvent {
  frame: ImageData;
  index: number;
}
@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss'],
})

export class CameraComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('video') video: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;

  // TODO depending on the screen orientation, toggle width and height

  @Input() width = 480;
  @Input() height = 640;
  @Input() opened = false;
  @Output() update = new EventEmitter<CameraEvent>();

  cameraWidth = 640;
  cameraHeight = 480;
  loading = true;
  fps = 30;
  facingMode = 'environment';
  frame: ImageData;
  index = 0;
  interval: any;

  constructor() {
    console.log('constructor');

  }


  ngOnInit() {
    console.log('ngOnInit');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    this.startCamera();
  }

  startCamera() {
    const video = this.video.nativeElement;
    video.muted = true;
    navigator.mediaDevices.getUserMedia({ video: { facingMode: this.facingMode } })
      .then(stream => {
        video.srcObject = stream;
        video.setAttribute('playsinline', '');
        video.play();
        this.opened = true;
        this.tick();
      });
  }

  async tick() {

    if (!this.opened) {
      this.ngOnDestroy();
      return;
    }

    const video = this.video.nativeElement;
    const canvas = this.canvas.nativeElement;
    const context = canvas.getContext('2d');

    if (this.frame) {
      this.loading = false;
      context.putImageData(this.frame, 0, 0);
    }

    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    this.frame = context.getImageData(0, 0, canvas.width, canvas.height);

    this.interval = setTimeout(async () => {
      await this.tick();
      const event: CameraEvent = { frame: this.frame, index: this.index };
      this.update.emit(event);
      this.index++;
    }, 1000 / this.fps);
  }

  ngOnDestroy() {
    console.log('ngOnDestroy');

    this.opened = false;

    clearInterval(this.interval);
    if (this.video.nativeElement.srcObject) {
      this.video.nativeElement.srcObject.getTracks().forEach((track: { stop: () => any }) => track.stop());
    }
  }

}
