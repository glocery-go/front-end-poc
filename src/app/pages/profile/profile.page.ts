import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { User } from 'src/app/core/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  @ViewChild('pwaphoto', { static: false }) pwaphoto: ElementRef;

  public imageURI: string;
  public photoId: string;
  public sectorId: number;
  public guestName: string;

  public user: User = new User();

  constructor() {
    console.log('c');
  }

  ngOnInit(): void {
    this.guestName = localStorage.getItem('guest');
  }
}
