import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public isLogin: boolean;
  public guestName: string;

  constructor(
    private router: Router,
    private alertController: AlertController
  ) { }

  loginPanel() {
    this.isLogin = !this.isLogin;
  }

  guest() {

    if (!this.guestName) {
      this.alertController.create({
        header: 'Ops!',
        message: 'Esqueceu de digitar seu nome :)',
        buttons: [
          {
            text: 'Ok',
            cssClass: 'primary',
            // handler: () => { } // TODO implement this handler
          }]
      }).then(alert => alert.present());
    }

    if (this.guestName) {
      localStorage.setItem('guest', this.guestName);
      this.router.navigateByUrl(`/side-menu/hometabs/tabs/menu-tab`);
    }
  }

}
