import { Component, OnInit, Inject } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { User } from 'src/app/core/models/user';
import { HttpClient } from '@angular/common/http';
import { PAGE_LIST } from './page-list';
import packageJson from '../../../../package.json';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.page.html',
  styleUrls: ['./side-menu.page.scss'],
})

export class SideMenuPage implements OnInit {

  public user: User;
  public imageURI: string;
  public selectPath: string;
  public pages = PAGE_LIST;
  public version = packageJson.version;

  constructor(
    private http: HttpClient,
    private route: Router,
    @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {


    this.selectPath = '';
    this.route.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectPath = event.url;
      }
    });
  }

  logOut(): void {
    this.http.get('todo');
  }

  // getProfileImage(user: User): any {
  //   if (this.user.login != null && this.user.login.length === 6) {
  //     this.imageService.getBySSB(this.user.login).subscribe((result) => {
  //       this.picture = result.content;
  //       this.user.profileImage = `data:image/png;base64,` + this.picture[0].foto;
  //     });
  //   } else {
  //     this.picture = null;
  //   }
  // }
}
