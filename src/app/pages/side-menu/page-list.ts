const urlLogout = 'https://fg.ciam.preprod.aws.scania.com/auth/realms/scania/protocol/openid-connect/logout?redirect_uri=';
const redirectUri = encodeURI('https://digitalqlist.br.scania.com');

const pages = [
  {
    title: 'Home',
    url: '/side-menu/hometabs',
    icon: 'home'
  },
  {
    title: 'Perfil',
    url: '/side-menu/profile',
    icon: 'person'
  },
  {
    title: 'Histórico',
    url: '/side-menu/historic',
    icon: 'time'
  },
  {
    title: 'Configurações',
    url: '/side-menu/settings',
    icon: 'settings',
  },
  {
    title: 'Sair',
    url: urlLogout + redirectUri,
    icon: 'log-out',
    quit: true
  }
];

export const PAGE_LIST = pages;
