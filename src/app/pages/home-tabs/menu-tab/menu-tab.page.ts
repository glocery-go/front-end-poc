import { Component, Compiler } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-menu-tab',
  templateUrl: './menu-tab.page.html',
  styleUrls: ['./menu-tab.page.scss'],
})
export class MenuTabPage {
  constructor(
    private router: Router,
    private compiler: Compiler,
    private toastCrtl: ToastController,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private alertController: AlertController
  ) {
    const isInStandaloneMode = () => 'standalone' in window.navigator;

    if (this.platform.is('ios') && isInStandaloneMode()) {
      console.log('Im a an iOS PWA');
    }
  }

  async doRefresh(event: any): Promise<void> {
    this.compiler.clearCache();
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }
}
