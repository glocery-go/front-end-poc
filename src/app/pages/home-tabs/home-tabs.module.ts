import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeTabsPage } from './home-tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: HomeTabsPage,
    children: [
      {
        path: 'menu-tab',
        loadChildren: () => import('../home-tabs/menu-tab/menu-tab.module').then(m => m.MenuTabPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/menu-tab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeTabsPage]
})
export class HomeTabsPageModule { }
