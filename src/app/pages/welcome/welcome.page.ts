import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  @ViewChild('welcomeSlider', { static: true }) slides: IonSlides;
  public guestName: string;

  ngOnInit() {
    this.guestName = localStorage.getItem('guest');
  }

  swipeNext() {
    this.slides.slideNext();
  }
}
