import { Injectable } from '@angular/core';
import { AlertButton, AlertController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    private readonly text = 'OK';
    private readonly cssClass = 'primary';

    constructor(private alertController: AlertController) { }

    alert<T>(
        message: string,
        header?: string,
        text: string = this.text,
        cssClass: string = this.cssClass,
        callback?: (value: T) => boolean | void | { [key: string]: T } | Promise<void>,
        buttons: (AlertButton | string)[] = [{ text, cssClass, handler: callback }]
    ): Promise<void> {
        return this.alertController.create({
            header, message, buttons
        }).then(alert => {
            alert.present();
        });
    }
}
