export const tryParseInt: any = (str: any, defaultValue: number) => {

  let retValue = defaultValue;
  if (str !== null) {
    if (str.length > 0) {
      if (!isNaN(str)) {
        retValue = parseInt(str, 10);
      }
    }
  }
  return retValue;
};

export const dateToMySql: any = (date: string) => {
  const newDate = new Date(date);
  const dateToMysql = newDate.toISOString().slice(0, 19).replace('T', ' ');
  return dateToMysql;
};

export const formatDate: any = (date: string | number | Date) => {
  const d = new Date(date);
  let day = '' + d.getDate();
  let month = '' + (d.getMonth() + 1);
  const year = d.getFullYear();
  if (month.length < 2) {
    month = '0' + month;
  }
  if (day.length < 2) {
    day = '0' + day;
  }
  return [year, month, day].join('-');
};


export const firstFileToBase64: any = (fileImage: File) => new Promise((resolve, reject) => {

  const fileReader: FileReader = new FileReader();

  if (fileReader && fileImage != null) {
    fileReader.readAsDataURL(fileImage);
    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.onerror = (error) => {
      reject(error);
    };

  } else {
    reject(new Error('No file found'));
  }

});
