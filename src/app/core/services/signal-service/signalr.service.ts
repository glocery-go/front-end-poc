import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder, HubConnectionState, LogLevel } from '@microsoft/signalr';
import { from, Observable, Subject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public signalRStopOnDestroy = false;
  public baseUrl = environment.apiTaktUrl;
  private connection: HubConnection;
  private startedConnection: Promise<any>;

  constructor() {
    this.connection = new HubConnectionBuilder()
      .withUrl(`${this.baseUrl}/visualization`)
      .configureLogging(LogLevel.Information)
      .withAutomaticReconnect()
      .build();

    this.connection.onreconnecting((error: any) => {
      console.assert(this.connection.state === HubConnectionState.Reconnecting);
      console.log(`Connection lost due to error "${error}". Reconnecting.`);
    });

    this.connection.onreconnecting((connectionId: any) => {
      console.assert(this.connection.state === HubConnectionState.Reconnecting);
      console.log(`Connection reestablished. Connected with connectionId "${connectionId}`);
    });

    this.connection.onclose(async () => {
      if (!this.signalRStopOnDestroy) {
        alert('Conexão foi fechada. Tente atualizar a pagina para reiniciar a conexão!');
        await this.start();
      }
    });
  }

  public connect(): void {
    this.startedConnection = this.startConnection();
  }

  public on(method: string): Observable<any> {

    const subject = new Subject<any>();

    this.connection.on(method, data => subject.next(data));

    return subject.asObservable();
  }

  public invoke(method: string, ...data: any): Observable<any> {
    return from(this.startedConnection)
      .pipe(
        mergeMap(_ => this.connection.invoke(method, ...data)));
  }

  public async start(): Promise<void> {
    try {
      await this.connection.start();
      console.log('SignalR Connected.');
    } catch (err) {
      console.log(err);
      setTimeout(this.startConnection, 5000);
    }
  }

  public close(): void {
    try {
      this.signalRStopOnDestroy = true;
      this.connection.stop();
      console.log('SignalR Disconnected.');
    } catch (err) {
      console.log(err);
    }
  }

  private startConnection(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.connection
        .start()
        .then(() => {
          console.log(`SignalR connection success! connectionId: ${this.connection.connectionId} `);
          resolve('connected');
        })
        .catch(err => {
          console.log('Error while establishing connection, retrying...');
          console.log(`SignalR connection error: ${err}`);
          setTimeout(this.startConnection, 5000);
          reject('Not Connected');
        });
    });
  }
}
