// import { Injectable } from '@angular/core';
// import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
// import { environment } from '../../../../environments/environment';
// import { Observable } from 'rxjs';
// import { Profile } from '../../models/profile/profile';
// import { Role } from '../../models/profile/role';
// import Swal from 'sweetalert2';
// import { Result } from '../../models/result/result';

// @Injectable({
//   providedIn: 'root'
// })
// export class ProfileService {

//   constructor(private http: HttpClient) {
//     Swal.fire({
//       title: 'Carregando informações do usuário!',
//       allowEscapeKey: false,
//       allowOutsideClick: false,
//       didOpen: () => {
//         Swal.showLoading();
//         this.getLoggedUser().subscribe((result: Result<Profile>) => {
//           ProfileService.loggerUser = result.content;
//           console.log(ProfileService.loggerUser);
//           Swal.close();
//         }, (error) => {
//           console.log('Não foi possível obter o contexto do usuário!');
//           Swal.close();
//         });
//       }
//     });
//   }

//   public static loggerUser: Profile;

//   httpOptions = {
//     headers: new HttpHeaders({
//       'Content-Type': 'application/json',
//     })
//   };

//   baseUrl = environment.api_url;

//   public getAll(): Observable<Result<Profile[]>> {
//     return this.http.get<Result<Profile[]>>(`${this.baseUrl}/api/profile`);
//   }

//   public save(profile: Profile): Observable<Result<Profile>> {
//     return this.http.post<Result<Profile>>(`${this.baseUrl}/api/profile`, profile);
//   }

//   public update(profile: Profile): Observable<Result<Profile>> {
//     return this.http.patch<Result<Profile>>(`${this.baseUrl}/api/profile`, profile);
//   }

//   public delete(id: number): Observable<Result<Profile>> {
//     return this.http.delete<Result<Profile>>(`${this.baseUrl}/api/profile/${id}`);
//   }

//   public getLoggedUser(): Observable<Result<Profile>> {
//     return this.http.get<Result<Profile>>(`${this.baseUrl}/api/profile/getloggeduser`);
//   }

//   public getAllRoles(): Observable<Result<Role[]>> {
//     return this.http.get<Result<Role[]>>(`${this.baseUrl}/api/profile/getallroles`);
//   }

//   public getById(id: number): Observable<Result<Profile>> {
//     const params = new HttpParams().set('id', String(id));
//     return this.http.get<Result<Profile>>(`${this.baseUrl}/api/profile`, { params });
//   }

//   public getAllRelated(): Observable<Result<Profile[]>> {
//     return this.http.get<Result<Profile[]>>(`${this.baseUrl}/api/profile/getallrelated`);
//   }
// }
