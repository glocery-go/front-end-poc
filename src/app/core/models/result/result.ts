export class Result<T = void> {
    public success: boolean;
    public domainErrorMessages: string[];
    public content: T;
    public hasException: boolean;
    public exceptionMessage: string;
    public hasNotifications: boolean;
    public domainNotifications: string[];
}
