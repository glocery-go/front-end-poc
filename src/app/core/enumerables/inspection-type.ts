const enum ExampleTypeEnum {
    a = 'manual',
    b = 'visual',
    c = 'camera',
    d = 'ocr'
}
// TODO how to ignore enum camelcase in linters?

export default ExampleTypeEnum;
