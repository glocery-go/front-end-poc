import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PwaUpdateService {

  constructor(
    public updates: SwUpdate,
    private toastController: ToastController
  ) {
    const intervalTime = 60 * 60 * 6;
    if (updates.isEnabled) {
      console.log('Registrando atualizador PWA');
      interval(intervalTime).subscribe(() => updates.checkForUpdate()
        .then(() => console.log('checking for updates')));
    }
  }

  public checkForUpdates(): void {
    this.updates.available.subscribe(event => this.promptUser());
  }

  async showUpdateMessage() {
    const toast = await this.toastController.create({
      message: 'Existe uma nova versão deste App disponível',
      duration: 10000,
      buttons: [{
        text: 'Atualizar',
        handler: () => {
          /* tslint:disable */
          window.location.reload();
          /* tslint:enable */
        }
      }]
    });

    toast.present();
  }

  private promptUser(): void {
    console.log('updating to new version');
    this.updates.activateUpdate().then(() => this.showUpdateMessage());
  }
}
