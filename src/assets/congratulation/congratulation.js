
function congratulation(){
  var numberOfStars = 10;
	
	for (var i = 0; i < numberOfStars; i++) {
	  $('.congrats').append('<div class="blob fa fa-star ' + i + '"></div>');
	}	

	animateText();
	animateBlobs();
}

function reset() {
	$.each($('.blob'), function(i) {
		TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
	});	
	TweenMax.set($('h1'), { scale: 1, opacity: 1, rotation: 0 });
}

function animateText() {
		TweenMax.from($('h1'), 0.8, {
		scale: 0.4,
		opacity: 0,
		rotation: 15,
		ease: Back.easeOut.config(4),
	});
}
	
function animateBlobs() {	

	var xSeed = Math.random() * (380 - 350) + 350;
	var ySeed =  Math.random() * (170 - 120) + 120;
		
	$.each($('.blob'), function( i) {
		var blob = $(this);
		var speed = Math.random() * (5 - 1) + 1;
		var rotation = Math.random() * (100 - 5) + 5;
		var scale =  Math.random() * (1.5 - 0.8) + 0.8;
		var x =  Math.random() * (ySeed -(-ySeed)) + (-ySeed);
		var y =  Math.random() * (ySeed - (-ySeed)) + (-ySeed);

		TweenMax.to(blob, speed, {
			x: x,
			y: y,
			ease: Power1.easeOut,
			opacity: 1,
			rotation: rotation,
			scale: scale,
			onStartParams: [blob],
			onStart: function($element) {				
				$element.css('display', 'block');
			},
			onCompleteParams: [blob],
			onComplete: function($element) {
				$element.css('display', 'none');
			}
		});
	});
}